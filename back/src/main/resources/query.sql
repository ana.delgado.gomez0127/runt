INSERT INTO Colegio (Id, Nombre) VALUES (1, 'El colegio del Olimpo');

INSERT INTO Curso (Id, Grado, Salon, Colegio_id) VALUES (1, 10, 'A', 1);
INSERT INTO Curso (Id, Grado, Salon, Colegio_id) VALUES (2, 10, 'B', 1);
INSERT INTO Curso (Id, Grado, Salon, Colegio_id) VALUES (3, 11, 'A', 1);
INSERT INTO Curso (Id, Grado, Salon, Colegio_id) VALUES (4, 11, 'B', 1);

INSERT INTO Profesor (Id, Nombre) VALUES (1, 'Némesis');
INSERT INTO Profesor (Id, Nombre) VALUES (2, 'Príapo');
INSERT INTO Profesor (Id, Nombre) VALUES (3, 'Iris');

INSERT INTO Asignatura (Id, Nombre, Curso_id, Profesor_id) VALUES (1, 'Matemáticas', 1, 1);
INSERT INTO Asignatura (Id, Nombre, Curso_id, Profesor_id) VALUES (2, 'Español', 1, 2);
INSERT INTO Asignatura (Id, Nombre, Curso_id, Profesor_id) VALUES (3, 'Ingles básico', 1, 3);
INSERT INTO Asignatura (Id, Nombre, Curso_id, Profesor_id) VALUES (4, 'Matemáticas', 2, 1);
INSERT INTO Asignatura (Id, Nombre, Curso_id, Profesor_id) VALUES (5, 'Español', 2, 2);
INSERT INTO Asignatura (Id, Nombre, Curso_id, Profesor_id) VALUES (6, 'Ingles avanzado', 2, 3);
INSERT INTO Asignatura (Id, Nombre, Curso_id, Profesor_id) VALUES (7, 'Matemáticas', 3, 1);
INSERT INTO Asignatura (Id, Nombre, Curso_id, Profesor_id) VALUES (8, 'Pre Icfes', 3, 1);
INSERT INTO Asignatura (Id, Nombre, Curso_id, Profesor_id) VALUES (9, 'Matemáticas', 4, 1);
INSERT INTO Asignatura (Id, Nombre, Curso_id, Profesor_id) VALUES (10, 'Pre Icfes', 4, 1);

INSERT INTO Estudiante (Id, Nombre) VALUES (1, 'Afrodita');
INSERT INTO Estudiante (Id, Nombre) VALUES (2, 'Apolo');
INSERT INTO Estudiante (Id, Nombre) VALUES (3, 'Ares');
INSERT INTO Estudiante (Id, Nombre) VALUES (4, 'Artemisa');
INSERT INTO Estudiante (Id, Nombre) VALUES (5, 'Atenea');
INSERT INTO Estudiante (Id, Nombre) VALUES (6, 'Dionisio');
INSERT INTO Estudiante (Id, Nombre) VALUES (7, 'Hefesto');
INSERT INTO Estudiante (Id, Nombre) VALUES (8, 'Hera');
INSERT INTO Estudiante (Id, Nombre) VALUES (9, 'Hermes');
INSERT INTO Estudiante (Id, Nombre) VALUES (10, 'Hades');
INSERT INTO Estudiante (Id, Nombre) VALUES (11, 'Poseidón');
INSERT INTO Estudiante (Id, Nombre) VALUES (12, 'Zeus');

INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (1, 1);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (1, 2);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (1, 3);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (2, 1);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (2, 2);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (2, 3);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (3, 1);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (3, 2);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (3, 3);

INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (4, 4);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (4, 5);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (4, 6);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (5, 4);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (5, 5);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (5, 6);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (6, 4);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (6, 5);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (6, 6);

INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (7, 7);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (7, 8);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (8, 7);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (8, 8);

INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (9, 9);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (9, 10);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (9, 11);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (9, 12);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (10, 9);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (10, 10);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (10, 11);
INSERT INTO ASIGNATURA_ESTUDIANTES (Asignatura_id, Estudiantes_id) VALUES (10, 12);