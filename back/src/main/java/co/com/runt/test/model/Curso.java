package co.com.runt.test.model;

import co.com.runt.test.Constants;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = Constants.ENTITY_TABLA_CURSO_NOMBRE)
public class Curso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = Constants.ENTITY_TABLA_CURSO_COLUMNA_GRADO)
    private int grado;

    @Column(name = Constants.ENTITY_TABLA_CURSO_COLUMNA_SALON)
    private String salon;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = Constants.ENTITY_TABLA_CURSO_COLUMNA_MAPPED_CURSO)
    @JsonBackReference
    private List<Asignatura> asignaturas;

    @ManyToOne
    @JoinColumn(name = Constants.ENTITY_TABLA_CURSO_COLUMNA_FK_COLEGIO, nullable = false, updatable = false)
    @JsonBackReference
    private Colegio colegio;
}
