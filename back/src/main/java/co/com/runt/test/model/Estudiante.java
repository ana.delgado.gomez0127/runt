package co.com.runt.test.model;

import co.com.runt.test.Constants;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = Constants.ENTITY_TABLA_ESTUDIANTE_NOMBRE)
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = Constants.ENTITY_TABLA_ESTUDIANTE_COLUMNA_NOMBRE)
    private String nombre;

    @ManyToMany
    @JoinColumn(name = Constants.ENTITY_TABLA_ESTUDIANTE_COLUMNA_FK_ASIGNATURA, nullable = false, updatable = false)
    @JsonBackReference
    private List<Asignatura> asignaturas;
}
