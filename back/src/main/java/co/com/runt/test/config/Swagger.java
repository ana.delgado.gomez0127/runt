package co.com.runt.test.config;

import co.com.runt.test.Constants;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Swagger {

    @Value("${application-version}")
    String appVersion;

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info()
                    .title(Constants.SWAGGER_TITULO)
                    .version(appVersion)
                    .description(Constants.SWAGGER_DESCRIPCION)
                    .termsOfService(Constants.SWAGGER_TERMINOS)
                        .contact(
                                new Contact()
                                        .name(Constants.SWAGGER_CONTACTO_NOMBRE)
                                        .email(Constants.SWAGGER_CONTACTO_EMAIL)
                        )
                    .license(new License()
                            .name(Constants.SWAGGER_LICENCIA_NOMBRE)
                            .url(Constants.SWAGGER_LICENCIA_URL)
                    )
                );
    }
}
