package co.com.runt.test;

public class Constants {

    /***** ENTITY PROFESOR *****/
    public static final String ENTITY_TABLA_COLEGIO_NOMBRE = "Colegio";
    public static final String ENTITY_TABLA_COLEGIO_COLUMNA_NOMBRE = "Nombre";
    public static final String ENTITY_TABLA_COLEGIO_COLUMNA_MAPPED_COLEGIO = "colegio";

    public static final String ENTITY_TABLA_CURSO_NOMBRE = "Curso";
    public static final String ENTITY_TABLA_CURSO_COLUMNA_GRADO = "Grado";
    public static final String ENTITY_TABLA_CURSO_COLUMNA_SALON = "Salon";
    public static final String ENTITY_TABLA_CURSO_COLUMNA_FK_COLEGIO = "Colegio_id";
    public static final String ENTITY_TABLA_CURSO_COLUMNA_MAPPED_CURSO= "curso";

    public static final String ENTITY_TABLA_ASIGNATURA_NOMBRE = "Asignatura";
    public static final String ENTITY_TABLA_ASIGNATURA_COLUMNA_NOMBRE = "Nombre";
    public static final String ENTITY_TABLA_ASIGNATURA_COLUMNA_FK_PROFESOR = "Profesor_id";
    public static final String ENTITY_TABLA_ASIGNATURA_COLUMNA_FK_CURSO = "Curso_id";
    public static final String ENTITY_TABLA_ASIGNATURA_COLUMNA_JOIN_ESTUDIANTE = "Estudiante_id";

    public static final String ENTITY_TABLA_PROFESOR_NOMBRE = "Profesor";
    public static final String ENTITY_TABLA_PROFESOR_COLUMNA_NOMBRE = "Nombre";
    public static final String ENTITY_TABLA_CURSO_COLUMNA_MAPPED_PROFESOR = "profesor";

    public static final String ENTITY_TABLA_ESTUDIANTE_NOMBRE = "Estudiante";
    public static final String ENTITY_TABLA_ESTUDIANTE_COLUMNA_NOMBRE = "Nombre";
    public static final String ENTITY_TABLA_ESTUDIANTE_COLUMNA_FK_ASIGNATURA = "Asignatura_id";

    /***** SWAGGER *****/
    public static final String SWAGGER_TITULO = "Colegio Olimpo";
    public static final String SWAGGER_DESCRIPCION = "Aplicación para manejar asignaturas, estudiantes y profesores";
    public static final String SWAGGER_TERMINOS = "http://swagger.io/terms/";
    public static final String SWAGGER_CONTACTO_NOMBRE = "Ana Milena Delgado Gomez";
    public static final String SWAGGER_CONTACTO_EMAIL = "ana.delgado.gomez0127@gmail.com";
    public static final String SWAGGER_LICENCIA_NOMBRE = "GNU";
    public static final String SWAGGER_LICENCIA_URL = "https://es.wikipedia.org/wiki/GNU_General_Public_License";

    public static final String SWAGGER_CONTROLLER_TITULO = "Controlador de funciones del Colegio";
    public static final String SWAGGER_CONTROLLER_DESCRIPCION = "Consulta las asignaturas y estudiantes segun el profesor indicado";

    public static final String MAPPING_CONTROLLER_BUSQUEDA_PROFESOR = "/consulta-profesor-nombre/{nombreProfesor}";
}
