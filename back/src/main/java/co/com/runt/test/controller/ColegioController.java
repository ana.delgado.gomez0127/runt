package co.com.runt.test.controller;

import co.com.runt.test.Constants;
import co.com.runt.test.model.Profesor;
import co.com.runt.test.service.ColegioService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = Constants.SWAGGER_CONTROLLER_TITULO, description = Constants.SWAGGER_CONTROLLER_DESCRIPCION)
public class ColegioController {

    @Autowired
    ColegioService colegioService;

    @GetMapping(value = Constants.MAPPING_CONTROLLER_BUSQUEDA_PROFESOR)
    public ResponseEntity<Profesor> busquedaProfesorPorNombre(@PathVariable String nombreProfesor){

        Profesor profesor = colegioService.consultaAsignaturasProfesor(nombreProfesor);

        if(profesor != null){
            return new ResponseEntity<>(profesor, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(profesor, HttpStatus.NO_CONTENT);
        }
    }

}
