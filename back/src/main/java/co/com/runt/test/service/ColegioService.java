package co.com.runt.test.service;

import co.com.runt.test.model.Profesor;
import co.com.runt.test.repository.ProfesorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class ColegioService {

    @Autowired
    ProfesorRepository profesorRepository;

    public Profesor consultaAsignaturasProfesor(String nombreProfesor){

        Optional<Profesor> profesor = profesorRepository.findByNombre(nombreProfesor);

        if(profesor.isPresent()){
            log.info("Profesor encontrado con el nombre {}, datos: {}", nombreProfesor, profesor.get().getId());
            return profesor.get();
        }
        else{
            log.info("Profesor no encontrado bajo busqueda del nombre: {}", nombreProfesor);
            return null;
        }

    }
}
