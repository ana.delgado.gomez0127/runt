package co.com.runt.test.model;

import co.com.runt.test.Constants;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = Constants.ENTITY_TABLA_COLEGIO_NOMBRE)
public class Colegio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = Constants.ENTITY_TABLA_COLEGIO_COLUMNA_NOMBRE)
    private String nombre;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = Constants.ENTITY_TABLA_COLEGIO_COLUMNA_MAPPED_COLEGIO)
    private List<Curso> cursos;
}
