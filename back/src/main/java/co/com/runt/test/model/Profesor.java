package co.com.runt.test.model;

import co.com.runt.test.Constants;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = Constants.ENTITY_TABLA_PROFESOR_NOMBRE)
public class Profesor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = Constants.ENTITY_TABLA_PROFESOR_COLUMNA_NOMBRE)
    private String nombre;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = Constants.ENTITY_TABLA_CURSO_COLUMNA_MAPPED_PROFESOR)
    private List<Asignatura> asignaturas;
}
