package co.com.runt.test.model;

import co.com.runt.test.Constants;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = Constants.ENTITY_TABLA_ASIGNATURA_NOMBRE)
public class Asignatura {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = Constants.ENTITY_TABLA_ASIGNATURA_COLUMNA_NOMBRE)
    private String nombre;

    @ManyToOne
    @JoinColumn(name = Constants.ENTITY_TABLA_ASIGNATURA_COLUMNA_FK_PROFESOR, nullable = false, updatable = false)
    @JsonBackReference
    private Profesor profesor;

    @ManyToMany
    @JoinColumn(name = Constants.ENTITY_TABLA_ASIGNATURA_COLUMNA_JOIN_ESTUDIANTE, nullable = false, updatable = false)
    private List<Estudiante> estudiantes;

    @ManyToOne
    @JoinColumn(name = Constants.ENTITY_TABLA_ASIGNATURA_COLUMNA_FK_CURSO, nullable = false, updatable = false)
    @JsonManagedReference
    private Curso curso;
}
