# RUNT - TEST
## _Introducción_

El proyecto presente en el siguiente repositorio, tiene como proposito cumplir con el reto técnico planteado por la empresa para continuar el proceso de selección en la compañia.

## Tecnologías

- Spring Boot - Java 11
- H2 (Base de datos en memoria)
- Swagger para documentación de proyecto.
- JPA (Java Persistence API) información relacional en base de datos.
- Lombok - Para generación de métodos Get, Set, ToString, Constructores, entre otros en base a variables.
- Javax Validations - Para uso de Constraint y validar algunos campos de los DTO (Data Transfer Object) en base a anotaciones.

## Instalación

Clonación de Repositorio.

```sh
git https://gitlab.com/ana.delgado.gomez0127/runt.git
```

Ejecución de Proyecto
```sh
gradle run
```
Generación de Artefacto
```sh
gradle build
```
## Base De Datos - H2

Una vez inicializado el proyecto, Hibernate genera las tablas en base a las Entity creadas.

Acceso a la consola de Hibernate:

```sh
Usuario: sa
URL: jdbc:h2:mem:h2test
http://localhost:8080/runt/console
```

![alt text here](https://i.postimg.cc/hjGVgnpd/Captura-de-Pantalla-2021-06-02-a-la-s-7-25-42-a-m.png)

## Documentación y Pruebas - SWAGGER
### _Vista General de Documentación_
![alt text here](https://i.postimg.cc/WzkFb1rh/Captura-de-Pantalla-2021-06-02-a-la-s-7-26-53-a-m.png)

### _Vista Especifica de Pruebas Controller_
![alt text here](https://i.postimg.cc/4xT2z8JS/Captura-de-Pantalla-2021-06-02-a-la-s-7-27-03-a-m.png)

### _Vista Especificaciones y Reglas de los Esquemas_
![alt text here](https://i.postimg.cc/TP399FBC/Captura-de-Pantalla-2021-06-02-a-la-s-7-27-22-a-m.png)

Visualización de Documentación y Pruebas del Servicio.
```sh
http://localhost:8080/runt/swagger.html
```

## Archivos Postman De Pruebas
Se adjunta imagen de muestra de las pruebas realizadas con postman, adicionalmente se incluye url de descarga

![alt text here](https://i.postimg.cc/rp4pvw6m/Captura-de-Pantalla-2021-06-02-a-la-s-7-29-36-a-m.png)

Descarga de Colección Postman
```sh
https://drive.google.com/file/d/105x2YDQYenRz4qxBVs0QYOWoadgw_MSj/view?usp=sharing
```

